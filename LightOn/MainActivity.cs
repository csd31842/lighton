﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;

using Android.Graphics;

namespace LightOn
{
    [Activity(Label = "LightOn", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        Switch button;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            button = FindViewById<Switch>(Resource.Id.btnLight);

            button.Click += SwitchLight;
        }

        public void SwitchLight(object obj, EventArgs e) {
            LinearLayout screen = FindViewById<LinearLayout>(Resource.Id.linearLayout1);

            if (button.Checked)
            {
                screen.SetBackgroundColor(Color.ParseColor("#fbd14b"));
            }
            else
            {
                screen.SetBackgroundColor(Color.ParseColor("#00162E"));
            }

        }
    }
}

